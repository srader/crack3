#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=32;        // Length of MD5 hash strings
const int STEP = 100;

int biglarry; // global non-const variable that counts elements in the dictionary. I could've done it some other way, but this is really easy.

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    
    // Compare the two hashes

    // Free any malloc'd memory

    
//    guess[strlen(guess) - 1] = '\0';
    guess = md5(guess, strlen(guess) );


    // Compare the two hashes
//    printf("Trying %s and %s\n", hash, guess);
    
    if(strcmp(hash, guess) == 0)
    {return 1;}
    else
    {return 0;}
    
    // Free any malloc'd memory
    //change code
}

// DONE
// Read in the hash file and return the array of strings.
char **read_hashes(char *filename)
{
    FILE *f = fopen(filename, "r");
    if(!f)
    {
        fprintf(stderr, "Can't open %s for reading\n", filename);
        return NULL;
    }
    
    int arrlen = 0;
    
    //allocate space for arrlen char *
    char **lines = NULL;
    
    char buf[1000];
    int i = 0;
    while (fgets(buf, 1000, f))
    {
        //check if array is full. If so, extend it.
        if(i == arrlen)
        {
            arrlen += STEP;
            
            char ** newline = (char **)realloc(lines, arrlen * sizeof(char *));
            if (!newline)
            {
                fprintf(stderr, "Can't realloc\n");
                exit(1);
            }
            lines = newline;
            
        }
        // trim off newline char
        buf[strlen(buf) - 1] = '\0';
        
        //get length of buf
        int slen = strlen(buf);
        
        //allocate space for the string
        char *str = (char *)malloc((slen+1) * sizeof(char));
        
        //copy string from buf to str
        strcpy(str, buf);
        
        //attach str to data structure
        lines[i] = str;
        i++;
    }
        
    
    if(i == arrlen)
    {
        char **newarr = (char **)realloc(lines, (arrlen+1)*sizeof(char *));
        if (!newarr)
        {
            fprintf(stderr, "can't realloc\n");
            exit(1);
        }
        
    }
    
    lines[i] = NULL;
    return lines;
}


// DONE
// Read in the dictionary file and return the data structure.
// Each entry should contain both the hash and the dictionary
// word.
char **read_dict(char *filename)
{
    FILE *f = fopen(filename, "r");
    if(!f)
    {
        fprintf(stderr, "Can't open %s for reading\n", filename);
        return NULL;
    }
    
    int arrlen = 0;
    
    //allocate space for arrlen char *
    char **lines = NULL;
    
    char buf[1000];
    int i = 0;
    while (fgets(buf, 1000, f))
    {
        //check if array is full. If so, extend it.
        if(i == arrlen)
        {
            arrlen += STEP;
            
            char ** newline = (char **)realloc(lines, arrlen * sizeof(char *));
            if (!newline)
            {
                fprintf(stderr, "Can't realloc\n");
                exit(1);
            }
            lines = newline;
            
        }
        // trim off newline char
        buf[strlen(buf) - 1] = '\0';
        
        //get length of buf
        int slen = strlen(buf) + HASH_LEN + 4;
        
        //allocate space for the string
        char *str = (char *)malloc((slen+1) * sizeof(char));
        
        //MD5 to str
        strcpy(str, md5(buf, strlen(buf)));
        
        //add middle bit
        strcat(str, " :: ");
        
        //copy string from buf to str
        strcat(str, buf);
        
        //attach str to data structure
        lines[i] = str;
        
        biglarry++;
        i++;
    }
        
    
    if(i == arrlen)
    {
        char **newarr = (char **)realloc(lines, (arrlen+1)*sizeof(char *));
        if (!newarr)
        {
            fprintf(stderr, "can't realloc\n");
            exit(1);
        }
        
    }
    
    lines[i] = NULL;
    return lines;
}


int compare(const void *a, const void *b)
{    
    return strcmp(*(char **)a, *(char **)b);
}



void binsearch(char *hash, char **dict)
{
    int high = 0;
    int low = biglarry - 1;
    int mid = (high + low) /2;
    
    while(strncmp(dict[high],dict[low],HASH_LEN) <= 0)
    {
        if(strncmp(dict[mid], hash, HASH_LEN) < 0)
        {
            high = mid +1;
        }
        else if (strncmp(dict[mid], hash, HASH_LEN) == 0)
        {
            printf("FOUND: %s\n", dict[mid]);
            break;
        }
        else
        {
            low = mid - 1;
        }
        
        mid = (high + low) / 2;    
    }
    

    if(high > low)
    {
        printf("HASH %s NOT IN DICTIONARY\n", hash);
    }
    
}




int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }


    // DONE: Read the hash file into an array of strings
    char **hashes = read_hashes(argv[1]);

    // DONE: Read the dictionary file into an array of strings
    char **dict = read_dict(argv[2]);
    
    
    
    // DONE: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    qsort(dict, biglarry, sizeof(char *), compare);
    

    // DONE
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    
    for(int h = 0; hashes[h] != NULL; h++)
    {
        binsearch(hashes[h], dict);
    }
    
}
